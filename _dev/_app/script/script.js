var JS = window.JS || {};

/**
 * @namespace Functionlaity to kick off the app behaviour 
 */
JS.goGoGadgetMap = {

	/**
	 * Initialisation function for all behaviour
	 */
	init: function() {
		JS.map.updateKey();
		JS.map.setupMap(); // Setup empty map so it is 'always' shown
		this.getData(); // First depedency, get the data to plot
	},
	/**
	 * Get the map data from an endpoint
	 */
	getData: function() {
		// For now we use google sheets
		// Callback within sheets calls passDataToMap()
		// Should really have some failover handling
		JS.sheets.init(Tabletop);
	},
	/**
	 * Hand on the map data to the map functions
	 * @param {object} mapData
	 */
	passDataToMap: function(mapData) {
		JS.map.init(mapData);
	}

};

// window.onload = function() {  

// };
window.addEventListener('DOMContentLoaded',function(){

	JS.goGoGadgetMap.init();

});