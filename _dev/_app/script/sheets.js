/*jshint sub:true*/

var JS = window.JS || { };

/**
 * @namespace Getting the data from Google Sheets via their API 
 */
JS.sheets = {

	publicSpreadsheetUrl : 'https://docs.google.com/spreadsheets/d/' + '1vby3AWoV6IZty6fu2OuJRLyERgSWcWBfkuSYz9CqWeE' + '/pubhtml',

	/**
	 * Initialisation function for all sheets behaviour
	 * intis a Tabletop object to get the data for us
	 * calls getSheets on success
	 */
	init: function() {

		Tabletop.init( { 
			key: JS.sheets.publicSpreadsheetUrl,
			callback: JS.sheets.parseSheets
		});
	
	},

	/*
	 * Take the worksheets and format them into a single object 
	 * that matches the schema we expect
	 * @param {object} data The returned data
	 * @param {object} tabletop The Tabletop object with associated methods
	 */
	parseSheets: function(data, tabletop) {

		// Get the names of the sheets returned
		// var names = tabletop.foundSheetNames;

		// process all sheets 
		// set sheets global so we can access on other methods
		var sheets = tabletop.sheets();

		// This is where we will populate our formatted data
		var sheetData = {};

		// Mapping of required headers to their sheet column headers
		var sheetHeaders = {

			countryName: "Country Name",
			countryCode: "Country Code",
			legislationStatus: "Legislation status",
			yearOfIntroduction: "Year of introduction",
			yearOfLastAmendment: "Year of last amendment",
			coveredBondsIssued: "Covered bonds issued",
			coveredBondsOutstanding: "Covered bonds outstanding",
			poolInCase: "Seperate administrator of cover pool in case of insolvency",
			// Coverage Data Sheet
			legislationMortgage: "Mortgage",
			legislationPublicSector: "Public sector",
			legislationMovableAsset: "Ship, plane or other moveable asset",
			legislationBankPrinciple: "Special bank principle",
			legislationAssetMatching: "Asset-liability matching",
			legislationLtvRatio: "Maximum LTV ratio (residential)",
			legislationCoverPool: "Seperate administrator of cover pool in case of insolvency",
      // Extended Data
      coveredBondScheme: "Covered bond scheme",
      yearOfIntro: "Year of intro",
      collateralComposition: "Collateral composition",
      residentialMortgageDebt: "Value of residential mortgage debt billions",
      ratingSpectrum: "Rating spectrum",
      frequencyCoverage: "Frequency of coverage",
      minimumOvercollateralisation: "Defined minimum overcollateralisation",
      coverPoolDerivatives: "Derivatives pledged to cover pool",
      coveredBondsDueInInsolvency: "Covered bonds become due in case of insolvency",
      voluntaryOvercollateralisation: "Protection of voluntary overcollateralisation in case of insolvency?",
      CoverPoolDataDisclosure: "Cover pool data disclosure based on national transparency template"

		};

		// Get and set Base country data
		for(var j = 0; j < sheets.map_countries.elements.length; j++) {

			countryCode = sheets.map_countries.elements[j]['Country Code'];

			var bondData = {

				legislationStatus: { 
					value: JS.sheets.dataFormatter(sheets.map_countries.elements[j][sheetHeaders.legislationStatus]), 
					title: JS.sheets.dataFormatter(sheetHeaders.legislationStatus)
				},
				yearOfIntroduction: { 
					value: JS.sheets.dataFormatter(sheets.map_countries.elements[j][sheetHeaders.yearOfIntroduction]), 
					title: JS.sheets.dataFormatter(sheetHeaders.yearOfIntroduction)
				},
				yearOfLastAmendment: { 
					value: JS.sheets.dataFormatter(sheets.map_countries.elements[j][sheetHeaders.yearOfLastAmendment]), 
					title: JS.sheets.dataFormatter(sheetHeaders.yearOfLastAmendment)
				},
				coveredBondsIssued: { 
					value: JS.sheets.dataFormatter(sheets.map_countries.elements[j][sheetHeaders.coveredBondsIssued]), 
					title: JS.sheets.dataFormatter(sheetHeaders.coveredBondsIssued)
				},
				coveredBondsOutstanding: { 
					value: JS.sheets.dataFormatter(sheets.map_countries.elements[j][sheetHeaders.coveredBondsOutstanding]), 
					title: JS.sheets.dataFormatter(sheetHeaders.coveredBondsOutstanding)
				},

			};

			sheetData[countryCode] = {

				countryStatus: JS.sheets.setCountryStatus(bondData),
				countryName: JS.sheets.dataFormatter(sheets.map_countries.elements[j]['Country']),
				extendedData: {
					// extended data to follow
				}

			};

			// add bond data to main sheet
			sheetData[countryCode].bondsData = bondData;

		}


    // Get and set country legislation coverage data
    for(var i = 0; i < sheets.map_extended_data.elements.length; i++) {

      countryCode = sheets.map_extended_data.elements[i]['Country Code'];

      var extendedData = {

        coveredBondScheme: { 
          value: JS.sheets.dataFormatter(sheets.map_extended_data.elements[i][sheetHeaders.coveredBondScheme]), 
          title: JS.sheets.dataFormatter(sheetHeaders.coveredBondScheme)
        },
        yearOfIntro: { 
          value: JS.sheets.dataFormatter(sheets.map_extended_data.elements[i][sheetHeaders.yearOfIntro]), 
          title: JS.sheets.dataFormatter(sheetHeaders.yearOfIntro)
        },
        collateralComposition: { 
          value: JS.sheets.dataFormatter(sheets.map_extended_data.elements[i][sheetHeaders.collateralComposition]), 
          title: JS.sheets.dataFormatter(sheetHeaders.collateralComposition)
        },
        residentialMortgageDebt: { 
          value: JS.sheets.dataFormatter(sheets.map_extended_data.elements[i][sheetHeaders.residentialMortgageDebt]), 
          title: JS.sheets.dataFormatter(sheetHeaders.residentialMortgageDebt)
        },
        ratingSpectrum: { 
          value: JS.sheets.dataFormatter(sheets.map_extended_data.elements[i][sheetHeaders.ratingSpectrum]), 
          title: JS.sheets.dataFormatter(sheetHeaders.ratingSpectrum)
        },
        frequencyCoverage: { 
          value: JS.sheets.dataFormatter(sheets.map_extended_data.elements[i][sheetHeaders.frequencyCoverage]), 
          title: JS.sheets.dataFormatter(sheetHeaders.frequencyCoverage)
        },
        minimumOvercollateralisation: { 
          value: JS.sheets.dataFormatter(sheets.map_extended_data.elements[i][sheetHeaders.minimumOvercollateralisation]), 
          title: JS.sheets.dataFormatter(sheetHeaders.minimumOvercollateralisation)
        },
        coverPoolDerivatives: { 
          value: JS.sheets.dataFormatter(sheets.map_extended_data.elements[i][sheetHeaders.coverPoolDerivatives]), 
          title: JS.sheets.dataFormatter(sheetHeaders.coverPoolDerivatives)
        },
        coveredBondsDueInInsolvency: { 
          value: JS.sheets.dataFormatter(sheets.map_extended_data.elements[i][sheetHeaders.coveredBondsDueInInsolvency]), 
          title: JS.sheets.dataFormatter(sheetHeaders.coveredBondsDueInInsolvency)
        },
        voluntaryOvercollateralisation: { 
          value: JS.sheets.dataFormatter(sheets.map_extended_data.elements[i][sheetHeaders.voluntaryOvercollateralisation]), 
          title: JS.sheets.dataFormatter(sheetHeaders.voluntaryOvercollateralisation)
        },
        CoverPoolDataDisclosure: { 
          value: JS.sheets.dataFormatter(sheets.map_extended_data.elements[i][sheetHeaders.CoverPoolDataDisclosure]), 
          title: JS.sheets.dataFormatter(sheetHeaders.CoverPoolDataDisclosure)
        }

      };

      sheetData[countryCode].extendedData = extendedData;

    }

		// Get and set country legislation coverage data
		for(var k = 0; k < sheets.map_coverage.elements.length; k++) {

			countryCode = sheets.map_coverage.elements[k]['Country Code'];

			var CoverageData = {

				legislationMortgage: { 
					value: JS.sheets.dataFormatter(sheets.map_coverage.elements[k][sheetHeaders.legislationMortgage]), 
					title: JS.sheets.dataFormatter(sheetHeaders.legislationMortgage)
				},
				legislationPublicSector: { 
					value: JS.sheets.dataFormatter(sheets.map_coverage.elements[k][sheetHeaders.legislationPublicSector]), 
					title: JS.sheets.dataFormatter(sheetHeaders.legislationPublicSector)
				},
				legislationMovableAsset: { 
					value: JS.sheets.dataFormatter(sheets.map_coverage.elements[k][sheetHeaders.legislationMovableAsset]), 
					title: JS.sheets.dataFormatter(sheetHeaders.legislationMovableAsset)
				},
				legislationBankPrinciple: { 
					value: JS.sheets.dataFormatter(sheets.map_coverage.elements[k][sheetHeaders.legislationBankPrinciple]), 
					title: JS.sheets.dataFormatter(sheetHeaders.legislationBankPrinciple)
				},
				legislationAssetMatching: { 
					value: JS.sheets.dataFormatter(sheets.map_coverage.elements[k][sheetHeaders.legislationAssetMatching]), 
					title: JS.sheets.dataFormatter(sheetHeaders.legislationAssetMatching)
				},
				legislationLtvRatio: { 
					value: JS.sheets.dataFormatter(sheets.map_coverage.elements[k][sheetHeaders.legislationLtvRatio]), 
					title: JS.sheets.dataFormatter(sheetHeaders.legislationLtvRatio)
				},
				legislationCoverPool: { 
					value: JS.sheets.dataFormatter(sheets.map_coverage.elements[k][sheetHeaders.legislationCoverPool]), 
					title: JS.sheets.dataFormatter(sheetHeaders.legislationCoverPool)
				}

			};

			sheetData[countryCode].coverageData = CoverageData;

		}


		// Pass the data on to the map
		JS.goGoGadgetMap.passDataToMap(sheetData);

	},

	/*
	 * For each country check the minimum data requirements are met
	 * @param {object} bondData Core subset of the country data
	 * @returns {boolean} 0 = fail 1 = pass
	 */
	setCountryStatus: function(bondData){

		var score = [];
		var minimumScore = 1;

		if(bondData.legislationStatus.value){
			score.push(1);
		}

    // if(bondData.coveredBondsIssued.value){
    //   score.push(1);
    // }

    // if(bondData.coveredBondsOutstanding.value){
    //   score.push(1);
    // }    

		if(score.length < minimumScore){
			return 0;
		}else{
			return 1;
		}

	},

	/*
	 * Pass through function to be completed later 
	 * in order to ensure data is correctly escaped
	 * @param {object} data A data item to validate
	 * @returns {object} Input data escaped as required
	 */
	dataFormatter: function(data){

		// Regex to follow

		return data;

	}

};