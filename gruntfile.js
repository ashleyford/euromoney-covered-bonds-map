module.exports = function(grunt) {

	// node modules to automatically include dependencies
	require('matchdep').filterDev('grunt-*').forEach(grunt.loadNpmTasks);

	var globalConfig =  {

		path_dev_root: '_dev/',
		path_dist_root: '_public/',
		// The path to our assets within the output directory
		path_assets_stub: 'assets/', // Change this path as appropriate for your project

		path_dev_bower: '<%= globalConfig.path_dev_root %>bower_components/',
		path_dev_app: '<%= globalConfig.path_dev_root %>_app/',
		path_dev_css: '<%= globalConfig.path_dev_app %>style/',
		path_dev_js: '<%= globalConfig.path_dev_app %>script/',
		path_dev_img: '<%= globalConfig.path_dev_app %>img/',

		path_dist_assets: '<%= globalConfig.path_dist_root %><%= globalConfig.path_assets_stub %>',
		path_dist_css: '<%= globalConfig.path_dist_assets %>style/',
		path_dist_js: '<%= globalConfig.path_dist_assets %>script/',
		path_dist_img: '<%= globalConfig.path_dist_assets %>img/',
		path_dist_fonts: '<%= globalConfig.path_dist_assets %>fonts/',

		lib_scripts: [
			'<%= globalConfig.path_dev_bower %>ammap3/ammap/ammap.js',
			'<%= globalConfig.path_dev_bower %>ammap3/ammap/maps/js/worldHigh.js',
			'<%= globalConfig.path_dev_bower %>ammap3/ammap/themes/light.js',
			'<%= globalConfig.path_dev_bower %>jquery/dist/jquery.min.js',
			'<%= globalConfig.path_dev_bower %>tabletop/src/tabletop.min.js'
		],
		scripts: [
			'<%= globalConfig.path_dev_js %>*.js'
		],
	};

	// Project configuration.
	grunt.initConfig({

		globalConfig: globalConfig,

		// Spin up a local Node server on http://localhost:3001
		express: {
			dist: {
				options: {
					server: 'express-server.js',
					bases: ['_dist'],
					port: 3001,
					hostname: "*",
					livereload: true
				}
			}
		},

		// Install our bower dependancies
		bower: {
			install: {
				options: {
					copy: false // Defaults to true and copies bower files to /lib which we don't want
				}
			}
		},

		// Clean out the /dist directory to ensure only current files are present
		clean: {
			dist: ["<%= globalConfig.path_dist_root %>"]
		},

		// Process SCSS to CSS
		sass: {
			dist: {
				options: {
					style: 'compressed'
				},
				files: {
					'<%= globalConfig.path_dist_css %>style.css': '<%= globalConfig.path_dev_css %>style.scss'
				}
			}
		},

		// Concatenate JS as specified in globalConfig
		concat: {   
			dist: {
				files: [
					{
						src: ['<%= globalConfig.lib_scripts %>','<%= globalConfig.scripts %>'],
						dest: '<%= globalConfig.path_dist_js %>script.js',
					}
				]
			}
		},

		// Minify Concatenated JS
		uglify: {
			options: {
				banner: '/* Build: <%= grunt.template.today("yyyy-mm-dd") %> */'
			},
			dist: {
				files: [{
					expand: true,
					cwd: '<%= globalConfig.path_dist_js %>',
					src: '*.js',
					dest: '<%= globalConfig.path_dist_js %>'
				}]
			}
		},

		// JS Hinting
		jshint: {
			// Check Grunt itself is ok
			grunt: [ 
				'Gruntfile.js'
			],
			// Before concatenation
			dist_pre: '<%= globalConfig.scripts %>',
			// after concatenation and minification
			dist_post: [ 
				'<%= uglify.dist.dest %>',
			]
		},

		// Optimise images + svgs and copy to /dist
		imagemin: {
			dist: { 
				files: [{
					expand: true,
					cwd: '<%= globalConfig.path_dev_app %>img/',
					src: ['**/*.{png,jpg,gif,ico}'],
					dest: '<%= globalConfig.path_dist_img %>'
				}]
			}
		},
		svgmin: {
			options: {
				plugins: [
					// Full list of plugins incase you need to disable any 
					// https://github.com/svg/svgo/tree/master/plugins
					{
						removeViewBox: false
					}, {
						removeUselessStrokeAndFill: false
					}
				]
			},
			dist: {
				files: [
					// We specify the /img folder as we don't want the task to pick up on SVG font files if they are present
					{
						expand: true,
						cwd: '<%= globalConfig.path_dev_app %>img/',
						src: ['**/*.svg'],
						dest: '<%= globalConfig.path_dist_img %>'
					}
				]
			}
		},


		copy: {
			// Make a copy of css files with the extension .scss so we can use @import
			bower_cssAsScss: {
				files: [
					{
						expand: true,
						cwd: '<%= globalConfig.path_dev_bower %>',
						src: ['**/*.css', '!**/*.min.css'],
						dest: '<%= globalConfig.path_dev_bower %>',
						filter: 'isFile',
						ext: ".scss"
					}
				]
			},
			// Copy the index file from the dev folder to be included in dist
			index: {
				files: [{
					expand: true,
					cwd: '<%= globalConfig.path_dev_root %>',
					src: ['index.html'],
					dest: '<%= globalConfig.path_dist_root %>'
				}]
			}
		},

		// Watch files for changes and apply if changed
		watch: {
			options: {
				livereload: true,
			},
			css: {
				files: ['<%= globalConfig.path_dev_css %>**/*.scss'],
				tasks: ['sass:dist']
			},
			js: {
				files: ['<%= globalConfig.path_dev_js %>**/*.js'],
				tasks: ['jshint:dist_pre','concat:dist','uglify:dist']
			},
			images: {
				files: ['<%= globalConfig.path_dev_app %>img/**/*.{png,jpg,gif,ico}'],
				tasks: ['newer:imagemin:dist']
			},
			svgs: {
				files: ['<%= globalConfig.path_dev_app %>img/**/*.svg'],
				tasks: ['newer:svgmin:dist']
			},
			copy_index: {
				files: ['<%= globalConfig.path_dev_root %>index.html'],
				tasks: ['copy:index']
			},
		}

	});

	// Default task(s).
	grunt.registerTask('_basics', [
		'bower:install',
		'clean:dist', 
		'imagemin:dist',
		// 'svg_sprites:dist',
		'svgmin:dist',
		// 'copy:bower_cssAsScss',
		'sass:dist', 
		'concat:dist', 
		'uglify:dist', 
		'jshint:dist_pre',
		'copy:index'
	]);

	grunt.registerTask('default', ['_basics','express','watch']);

};

